import path from "path";
import { Pact } from "@pact-foundation/pact";
import { boolean, integer, like, string } from "@pact-foundation/pact/src/dsl/matchers";
import todoService from '../../server/api/todo_service';


const provider = new Pact({
    consumer: process.env.CONSUMER_NAME,
    provider: process.env.PROVIDER_NAME,
    port: Number(process.env.PACT_BROKER_PORT),
    host: 'localhost',
    log: path.resolve(process.cwd(), 'pact/logs', 'mockserver-integration.log'),
    dir: path.resolve(process.cwd(), 'pact/pacts'),
    pactfileWriteMode: 'merge',
    cors: true,
    consumerVersion: '1.0.0',
    providerVersion: '1.0.0',
    branch: 'master',
});


describe('Todo Service Pact Tests', () => {

    beforeAll(() => provider.setup());

    afterAll(() => provider.finalize());

    beforeEach(() => provider.verify());

    describe('Get Todos Pact', () => {
        test('should get todos success response', async () => {

            const getTodosRequest = {
                method: 'GET',
                path: '/todos',
            };

            const getTodosSuccessfullResponse = {
                status: 200,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: {
                    data: like([
                        {
                            _id: string(),
                            title: string(),
                            description: string(),
                            completed: boolean(),
                            created_at: string(),
                        },
                        {
                            _id: string(),
                            title: string(),
                            description: string(),
                            completed: boolean(),
                            created_at: string(),
                        },
                    ]),
                    success: boolean(),
                    page: integer(),
                    limit: integer(),
                }

            }

            await provider.addInteraction({
                state: 'Get todos success',
                uponReceiving: 'a request for todos',
                withRequest: getTodosRequest,
                willRespondWith: getTodosSuccessfullResponse,
            });

            const response = await todoService.getTodos();

            //expect(response.data.data).toBe(getTodosSuccessfullResponse.body.data);

        });
    });

    describe('Get Todo Pact', () => {
        test('should get todo success response', async () => {

            const getTodoRequest = {
                method: 'GET',
                path: '/todos/62279cc56e21738dc7addcb0',
            };

            const getTodoSuccessfullResponse = {
                status: 200,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: {
                    data: {
                        _id: string(),
                        title: string(),
                        description: string(),
                        completed: boolean(),
                        created_at: string(),
                    },
                    success: boolean(),
                }

            }

            await provider.addInteraction({
                state: 'Get todo success',
                uponReceiving: 'a request for todo',
                withRequest: getTodoRequest,
                willRespondWith: getTodoSuccessfullResponse,
            });

            const response = await todoService.getTodo('62279cc56e21738dc7addcb0');

            expect(response.data.title).toEqual(getTodoSuccessfullResponse.body.data.title.contents);
        });
    });

    describe('Create Todo Pact', () => {
        test('should create todo success response', async () => {

            const createTodoRequest = {
                method: 'POST',
                path: '/todos',
                body: {
                    title: 'Buy Milk',
                    description: 'Milk for baby',
                },
            };

            const createTodoSuccessfullResponse = {
                status: 201,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: {
                    data: {
                        _id: string(),
                        title: string(),
                        description: string(),
                        completed: boolean(),
                        created_at: string(),
                    },
                    success: boolean(),
                }
            }

            await provider.addInteraction({
                state: 'Create todo success',
                uponReceiving: 'a request to create todo',
                withRequest: createTodoRequest,
                willRespondWith: createTodoSuccessfullResponse,
            });

            const response = await todoService.createTodo(createTodoRequest.body);

            expect(response.data.title).toEqual(createTodoSuccessfullResponse.body.data.title.contents);

        });
    });

    describe('Update Todo Pact', () => {
        test('should update todo success response', async () => {

            const updateTodoRequest = {
                method: 'PUT',
                path: '/todos/62279cc56e21738dc7addcb0',
                body: {
                    title: 'Buy Milk',
                    description: 'Milk for baby',
                },
            };

            const updateTodoSuccessfullResponse = {
                status: 200,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: {
                    data: {
                        _id: string(),
                        title: string(),
                        description: string(),
                        completed: boolean(),
                        created_at: string(),
                    },
                    success: boolean(),
                }
            }

            await provider.addInteraction({
                state: 'Update todo success',
                uponReceiving: 'a request to update todo',
                withRequest: updateTodoRequest,
                willRespondWith: updateTodoSuccessfullResponse,
            });

            const response = await todoService.updateTodo('62279cc56e21738dc7addcb0', updateTodoRequest.body);

            expect(response.data.title).toEqual(updateTodoSuccessfullResponse.body.data.title.contents);
        });
    });

    describe('Delete Todo Pact', () => {
        test('should delete todo success response', async () => {

            const deleteTodoRequest = {
                method: 'DELETE',
                path: '/todos/62279cc56e21738dc7addcb0',
            };

            const deleteTodoSuccessfullResponse = {
                status: 200,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: {
                    data: {
                        _id: string(),
                        title: string(),
                        description: string(),
                        completed: boolean(),
                        created_at: string(),
                    },
                    success: boolean(),
                }
            }

            await provider.addInteraction({
                state: 'Delete todo success',
                uponReceiving: 'a request to delete todo',
                withRequest: deleteTodoRequest,
                willRespondWith: deleteTodoSuccessfullResponse,
            });

            const response = await todoService.deleteTodo('62279cc56e21738dc7addcb0');

            expect(response.data.title).toEqual(deleteTodoSuccessfullResponse.body.data.title.contents);
        });
    });

    describe('Change Todo Completed Field', () => {
        test('should change todo completed field success response', async () => {



            const changeTodoCompletedFieldRequest = {
                method: 'PUT',
                path: '/todos/62279cc56e21738dc7addcb0/completed',
            };

            const changeTodoCompletedFieldSuccessfullResponse = {
                status: 200,
                body: {
                    data: boolean(),
                    success: true,
                }

            }

            await provider.addInteraction({
                state: 'Change todo completed field success',
                uponReceiving: 'a request to change todo completed field',
                withRequest: changeTodoCompletedFieldRequest,
                willRespondWith: changeTodoCompletedFieldSuccessfullResponse,
            });

            const response = await todoService.updateCompleted('62279cc56e21738dc7addcb0');

            expect(response.data).toEqual(changeTodoCompletedFieldSuccessfullResponse.body.data.contents);
        });
    })
})