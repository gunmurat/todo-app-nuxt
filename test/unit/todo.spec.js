import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex'

//Components
import TodoCard from '@/components/TodoCard.vue'
import TodoList from '@/components/TodoList.vue';
import TodoForm from '@/components/TodoForm.vue';
import CreateTodoForm from '@/components/CreateTodoForm.vue';
import AppHeader from '@/components/header/AppHeader.vue';

//Pages
import IndexPage from '@/pages/index.vue';
import TodosPage from '@/pages/todos/index.vue';
import EditTodoPage from '@/pages/todos/_id/index.vue';
import CreateTodoPage from '@/pages/todos/create-todo/index.vue';

const localVue = createLocalVue()
localVue.use(Vuex)

const mockTodos = [
    {
        _id: '5e9f8f8f8f8f8f8f8f8f8f8',
        title: 'test',
        completed: false,
        description: 'test',
        created_at: '2020-05-05T05:05:05.000Z',
    },
    {
        _id: '5e9f8f8f8f8f8f8f8f12312',
        title: 'test123',
        completed: true,
        description: 'test123',
        created_at: '2020-06-05T05:05:05.000Z',
    },
]

describe('TodoCard Component Tests', () => {
    let store
    let actions



    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,

        })
    });


    test('should TodoCard visible', () => {
        const wrapper = shallowMount(TodoCard, {
            localVue,
            propsData: {
                todo: mockTodos[0],
            }
        });
        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.find('.font-thin').text()).toBe('test')
    })

    test('should onChangeCompleted triggered', () => {
        const wrapper = shallowMount(TodoCard, {
            localVue,
            store,
            propsData: {
                todo: mockTodos[0],
            }
        });

        wrapper.find('#completed').trigger('click')

        expect(actions.updateCompleted).toHaveBeenCalled()
    })

    test('should onDelete triggered', () => {
        const wrapper = shallowMount(TodoCard, {
            localVue,
            store,
            propsData: {
                todo: mockTodos[0],
            }
        });

        wrapper.find('#delete').trigger('click')
        expect(actions.deleteTodo).toHaveBeenCalled()
    })

    test('should onEdit triggered', async () => {

        const mockRoute = {
            params: {
                id: '5e9f8f8f8f8f8f8f8f8f8f8',
            }
        }
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(TodoCard, {
            localVue,
            store,
            propsData: {
                todo: mockTodos[0],
            },
            mocks: {
                $router: mockRouter,
                $route: mockRoute,
            }
        });

        await wrapper.find('#edit').trigger('click')

        expect(mockRouter.push).toHaveBeenCalled()
        expect(mockRouter.push).toHaveBeenCalledWith({ "path": "/todos/5e9f8f8f8f8f8f8f8f8f8f8" })
    })
})

describe('TodoList Component Test', () => {
    test('should TodoList visible', () => {
        const wrapper = shallowMount(TodoList, {
            localVue,
            propsData: {
                todos: mockTodos,
            }
        });

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.findAllComponents(TodoCard).length).toBe(2)
    });
})

describe('TodoForm Component Test', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,

        })
    });


    test('should TodoForm visible', () => {
        const wrapper = shallowMount(TodoForm, {
            localVue,
            propsData: {
                todo: mockTodos[1],
            }
        });

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.find('#title').element.value).toBe(mockTodos[1].title)

    });

    test('should onEditTodo triggered', async () => {
        const mockRoute = {
            params: {
                id: '5e9f8f8f8f8f8f8f8f8f8f8',
            }
        }
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(TodoForm, {
            localVue,
            store,
            propsData: {
                todo: mockTodos[1],
            },
            mocks: {
                $router: mockRouter,
                $route: mockRoute,
            }
        });

        await wrapper.find('#edit').trigger('click')

        expect(mockRouter.push).toHaveBeenCalled()
        expect(mockRouter.push).toHaveBeenCalledWith('/')
        expect(actions.updateTodo).toHaveBeenCalled()

    })
})

describe('CreateTodoForm Component Test', () => {
    let store
    let actions

    const options = {
        data() {
            return {
                title: 'test',
                description: 'test-description',
            }
        },
    }

    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            addTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,

        })
    });

    test('should CreateTodoForm visible', () => {
        const wrapper = shallowMount(CreateTodoForm, {
            localVue,
            store,
        });


        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.find('#description').element.value).toBe('')
    });

    test('should onCreateTodo triggered', async () => {
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(CreateTodoForm, {
            localVue,
            store,
            mocks: {
                $router: mockRouter,
            }
        });

        await wrapper.find('#create').trigger('click')

        expect(mockRouter.push).toHaveBeenCalledWith('/')
        expect(actions.addTodo).toHaveBeenCalled()
    });
})

describe('AppHeader Component Tests', () => {
    test('should AppHeader visible', () => {
        const wrapper = shallowMount(AppHeader, {
            localVue,
        });

        expect(wrapper.isVisible()).toBe(true)
    });

    test('should onCreateTodo triggered', async () => {
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(AppHeader, {
            localVue,
            mocks: {
                $router: mockRouter,
            }
        });

        await wrapper.find('#create-todo').trigger('click')
        expect(mockRouter.push).toHaveBeenCalledWith({ "path": "/todos/create-todo" })
    })
})

describe('Index Page Unit Tests', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            addTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,

        })
    });

    test('should IndexPage visible', () => {
        const wrapper = shallowMount(IndexPage, {
            localVue,
            store,
        });

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.findAllComponents(TodoList).length).toBe(1)
    });

    test('should fetchTodos called in asyncData', async () => {
        const wrapper = shallowMount(IndexPage, {
            localVue,
            store,
        });

        // await wrapper.vm.$options.created({
        //     store,
        //     route: {
        //         path: '/',
        //     }
        // })

        expect(actions.fetchTodos).toHaveBeenCalled()
    })

})


describe('Todos Page Component Tests', () => {

    test('should TodosPage visible', () => {
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(TodosPage, {
            localVue,
            mocks: {
                $router: mockRouter,
            }
        });

        expect(wrapper.isVisible()).toBe(true)
        expect(mockRouter.push).toHaveBeenCalledWith('/')
    });
})

describe('Edit Todo Page Component Tests', () => {

    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            addTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            state: {
                todos: mockTodos,
            },
            getters: {
                getTodoById: state => id => {
                    return state.todos.find(todo => todo._id === id);
                },
            },
        })
    });


    test('should EditTodoPage visible', async () => {

        const mockRoute = {
            params: {
                id: '5e9f8f8f8f8f8f8f8f8f8f8',
            }
        }
        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(EditTodoPage, {
            localVue,
            store,
            mocks: {
                $router: mockRouter,
                $route: mockRoute,
            }
        });

        await wrapper.vm.$options.asyncData({
            store,
            route: mockRoute,

        })

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.findAllComponents(TodoForm).length).toBe(1)
        expect(wrapper.vm.$data.todo._id).toBe(mockTodos[0]._id)
        expect(actions.fetchSingleTodo).toHaveBeenCalled()

    });


})

describe('Create Todo Page Component Tests', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchTodos: jest.fn(),
            fetchSingleTodo: jest.fn(),
            addTodo: jest.fn(),
            updateTodo: jest.fn(),
            deleteTodo: jest.fn(),
            updateCompleted: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            state: {
                todos: mockTodos,
            },
            getters: {
                getTodoById: state => id => {
                    return state.todos.find(todo => todo._id === id);
                },
            },
        })
    });

    test('should CreateTodoPage visible', () => {

        const mockRouter = {
            push: jest.fn()
        }

        const wrapper = shallowMount(CreateTodoPage, {
            localVue,
            store,
            mocks: {
                $router: mockRouter,
            }
        });

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.findAllComponents(CreateTodoForm).length).toBe(1)

    })
})

