import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:5000',
    timeout: 1000,
});

async function getTodos() {
    const todos = await instance.get('/todos');
    return todos.data;
}

async function getTodo(id) {
    const todo = await instance.get(`/todos/${id}`);
    return todo.data;
}

async function createTodo(todo) {
    const newTodo = await instance.post('/todos', todo);
    return newTodo.data;
}

async function deleteTodo(id) {
    const todo = await instance.delete(`/todos/${id}`);
    return todo.data;
}

async function updateTodo(id, todo) {
    const updatedTodo = await instance.put(`/todos/${id}`, todo);
    if (updatedTodo.status === 200) {
        return updatedTodo.data;
    }
    return null;
}

async function updateCompleted(id) {
    const todo = await instance.put(`/todos/${id}/completed`);
    return todo.data;
}

export default {
    instance,
    getTodos,
    getTodo,
    createTodo,
    deleteTodo,
    updateTodo,
    updateCompleted
}